# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="user for prowlarr"
ACCT_USER_HOME=/var/lib/prowlarr
ACCT_USER_ID=119
ACCT_USER_GROUPS=( prowlarr )

acct-user_add_deps
