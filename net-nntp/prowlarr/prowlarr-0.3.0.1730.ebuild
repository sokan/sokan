# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit systemd

SRC_URI="https://github.com/Prowlarr/Prowlarr/releases/download/v${PV}/Prowlarr.develop.${PV}.linux-core-x64.tar.gz"

DESCRIPTION="Prowlarr is an indexer manager/proxy built on the popular arr .net/reactjs base stack to integrate with your various PVR apps."
HOMEPAGE="https://prowlarr.com/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="bindist strip test"

RDEPEND="
	acct-group/prowlarr
	acct-user/prowlarr
	media-video/mediainfo
	dev-libs/icu
	net-misc/curl
	dev-util/lttng-ust
	dev-db/sqlite"

MY_PN=Prowlarr
S="${WORKDIR}/${MY_PN}"

src_install() {
	newconfd "${FILESDIR}/${PN}.conf" ${PN}
	newinitd "${FILESDIR}/${PN}.init" ${PN}

	keepdir /var/lib/${PN}
	fowners -R ${PN}:${PN} /var/lib/${PN}

	insinto /etc/${PN}
	insopts -m0660 -o ${PN} -g ${PN}

	insinto /etc/logrotate.d
	insopts -m0644 -o root -g root
	newins "${FILESDIR}/${PN}.logrotate" ${PN}

	insinto /opt/${PN^}
	doins -r .
	fperms +x /opt/${PN^}/${PN^}

	systemd_dounit "${FILESDIR}/prowlarr.service"
	systemd_newunit "${FILESDIR}/prowlarr.service" "${PN}@.service"
}
